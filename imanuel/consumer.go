//BEANSTALKD CONSUMER

package main
import (
    "github.com/iwanbk/gobeanstalk"
    "log"
	//"reflect"
	"strings"
    "fmt"
    "github.com/googollee/go-gcm"
	apns "github.com/anachronistic/apns"
)

func main() {
    conn, err := gobeanstalk.Dial("localhost:11300")
    if err != nil {
        log.Fatal(err)
    }
    for {
        j, err := conn.Reserve()
        if err != nil {
            log.Fatal(err)
        }
        log.Printf("id:%d, body:%s\n", j.ID, string(j.Body))
		
		//SPLIT DATA
		result := strings.Split(string(j.Body), "#####CONCAT#####")
		// Display all elements.
		for i := range result {
			fmt.Println("result #",i, result[i])
		}
		// Length
		fmt.Println(len(result))
		
		//PUSH NOTIFICATION PROCESS//
		pushToAndroid(result[0],result[1])
		pushtoIOS(result[1])
		//pushToBaidu()
		//pushViaMessage()
		
        err = conn.Delete(j.ID) 
        if err != nil {
            log.Fatal(err)
        }
    }
}

func pushToAndroid(app_key string,device_token string){
    client := gcm.New(app_key)

    load := gcm.NewMessage(device_token)
    load.AddRecipient("abc")
    load.SetPayload("data", "1")
    load.CollapseKey = "demo"
    load.DelayWhileIdle = true
    load.TimeToLive = 10

    resp, err := client.Send(load)

    fmt.Printf("id: %+v\n", resp)
    fmt.Println("err:", err)
    fmt.Println("err index:", resp.ErrorIndexes())
    fmt.Println("reg index:", resp.RefreshIndexes())
}

func pushtoIOS(device_token string){
	payload := apns.NewPayload()
    payload.Alert = "intern:hellothere"
    payload.Badge = 1
    payload.Sound = "default"

    pn := apns.NewPushNotification()
    //pn.DeviceToken = "ff5407377561851dfd83d4e29a4f03b51b856edfd55b94329370ab4d1f177cdc";
    pn.DeviceToken = device_token

    pn.AddPayload(payload)
    client := apns.NewClient("gateway.sandbox.push.apple.com:2195", "PushDNS.pem", "PushDNSKey-noenc.pem")
    resp := client.Send(pn)

    alert, _ := pn.PayloadString()
    fmt.Println("Alert:", alert)
    fmt.Println("Success", resp.Success)
    fmt.Println("Error", resp.Error)
}

func pushToBaidu(){

}

func pushViaMessage(){

}