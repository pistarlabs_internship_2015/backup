package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
)

/*type ResponseRecorder struct {
        Code      int           // the HTTP response code from WriteHeader
        HeaderMap http.Header   // the HTTP response headers
        Body      *bytes.Buffer // if non-nil, the bytes.Buffer to append written data to
        Flushed   bool
        // contains filtered or unexported fields
}*/

func main() {
	handler := func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "something failed", http.StatusInternalServerError)
	}

	req, err := http.NewRequest("GET", "192.168.0.59:1300", nil)
	if err != nil {
		log.Fatal(err)
	}

	w := httptest.NewRecorder()
	handler(w, req)

	fmt.Printf("%d - %s", w.Code, w.Body.String())
}