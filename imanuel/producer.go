//API FILE

package main

import(
	"encoding/json"
	"fmt"
	"net/http"
 	"github.com/iwanbk/gobeanstalk"
    "log"
    "time"
    "gopkg.in/mgo.v2"
	//"gopkg.in/mgo.v2/bson"
)
//DATA REQUEST
type DataReq map[string]interface{}

type Data struct{
	Req DataReq
}

type Payload struct{
	DataRequest Data
}

type Requests struct {
        AppKey string
        DeviceToken string
        Message string
        Status bool
}

var isValid bool
var app_key=""
var device_token=""
var phone_number=""
var message=""
var type_device=""
var status bool
var TUBE_NAME string

func serveRest(w http.ResponseWriter, r *http.Request) {
	//VALIDATION BY SANTI//
	//...
	app_key = r.FormValue("app_key")
	device_token= r.FormValue("device_token")
	phone_number= r.FormValue("phone_number")
	message= r.FormValue("message")
	type_device= r.FormValue("type_device")
	status= false
	
	
	
	//fmt.Fprintf(w,string(response))
	fmt.Fprintf(w,"app_key: %s, device_token: %s, phone_number: %s, message: %s, type_device: %s",app_key,device_token, phone_number,message,type_device)

	fmt.Printf(r.URL.Path)
	isValid:=true //ASUMPTION DATA IS VALID	

	if isValid==true{
		//CALL METHOD JSON RESPONSE
		response, err:=getJsonResponse()
		if err!=nil{
			panic(err)
		}
		//SHOW JSON RESULT IN WEB
		fmt.Fprintf(w,"JSON")
		fmt.Fprintf(w, string(response))
		

		//CONNECTING TO MONGO DB & INSERTING DATA BY DWI//
		//....
		ConnectingToMongo(app_key,device_token,message,status)

		//WRITING TO BEANSTALKD BY IMANUEL//
		//CONCATE ALL DATA
		byt := []byte(app_key+"#####CONCAT#####"+device_token+"#####CONCAT#####"+phone_number+"#####CONCAT#####"+message+"#####CONCAT#####"+type_device)
	
		//CALL BEANSTALKD WRITER METHOD
		BeanstalkdReader(byt, "apns")
		/*
		var tube[2]string
		tube[0] = "apns"
		tube[1] = "gcm"
		
		for i := 0; i < 2;i++{
			TUBE_NAME:=tube[i]
			BeanstalkdReader(byt, TUBE_NAME)
			fmt.Printf(string(byt))
		}
		*/
		fmt.Printf("sukses")
	}
}
func Push(w http.ResponseWriter, r *http.Request) {
	//VALIDATION BY SANTI//
	//...
	app_key = r.FormValue("app_key")
	device_token= r.FormValue("device_token")
	phone_number= r.FormValue("phone_number")
	message= r.FormValue("message")
	type_device= r.FormValue("type_device")
	status= false
	
	//fmt.Fprintf(w,string(response))
	fmt.Fprintf(w,"app_key: %s, device_token: %s, phone_number: %s, message: %s, type_device: %s",app_key,device_token, phone_number,message,type_device)

	fmt.Printf(r.URL.Path)
	isValid:=true //ASUMPTION DATA IS VALID	

	if isValid==true{
		//CALL METHOD JSON RESPONSE
		response, err:=getJsonResponse()
		if err!=nil{
			panic(err)
		}
		//SHOW JSON RESULT IN WEB
		fmt.Fprintf(w,"JSON")
		fmt.Fprintf(w, string(response))
		

		//CONNECTING TO MONGO DB & INSERTING DATA BY DWI//
		//....
		ConnectingToMongo(app_key,device_token,message,status)

		//WRITING TO BEANSTALKD BY IMANUEL//
		//CONCATE ALL DATA
		byt := []byte(app_key+"#####CONCAT#####"+device_token+"#####CONCAT#####"+phone_number+"#####CONCAT#####"+message+"#####CONCAT#####"+type_device)
	
		//CALL BEANSTALKD WRITER METHOD
		BeanstalkdReader(byt, "apns")
		/*
		var tube[2]string
		tube[0] = "apns"
		tube[1] = "gcm"
		
		for i := 0; i < 2;i++{
			TUBE_NAME:=tube[i]
			BeanstalkdReader(byt, TUBE_NAME)
			fmt.Printf(string(byt))
		}
		*/
		fmt.Printf("sukses")
	}
}

func main() {
	http.HandleFunc("/",serveRest)
	http.HandleFunc("/push",Push)
	http.ListenAndServe("192.168.0.59:1300",nil)
	
}

func getJsonResponse()([]byte, error) {
	//INSERT DATA DUMMY TO MAP
	req:=make(map[string]interface{})
	req["app_key"]=app_key
    req["device_token"]=device_token
	req["phone_number"]=phone_number
	req["message"]=message
	req["type_device"]=type_device

    d:=Data{req}
    p:=Payload{d}

    //RETURN JSON RESPONSE
    return json.MarshalIndent(p,""," ")
}

func BeanstalkdReader(data []byte, TUBE_NAME string) {
    //CONNECT TO BEANSTALKD SERVER
    conn, err := gobeanstalk.Dial("localhost:11300")
    if err != nil {
        log.Fatal(err)
    }
    //INSERT DATA
	conn.Use(TUBE_NAME)
    id, err := conn.Put(data, 0, 1*time.Second, 0)
    if err != nil {
        log.Fatal(err)
    }
    log.Printf("Job id %d inserted\n", id)
}

func ConnectingToMongo(app_key string,device_token string, message string, status bool) {
	//connecting to mongoDB
	session, err := mgo.Dial("192.168.0.24:27017")
	if err != nil {
		panic(err)
    }

    defer session.Close()

    // Optional. Switch the session to a monotonic behavior.
    session.SetMode(mgo.Monotonic, true)

    //convenient access
    c := session.DB("Request").C("request")
    err = c.Insert(&Requests{app_key, device_token, message, status})
	if err != nil {
    	log.Fatal(err)
	}
}