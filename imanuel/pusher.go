package baidupush

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"time"
)

type Pusher struct {
	ApiKey     string
	SercretKey string
	Url        string
}

func NewPusher(apikey, secretkey, url string) *Pusher {
	return &Pusher{ApiKey: apikey, SercretKey: secretkey, Url: url}
}

//devicetype 1：Browser；2：PC；3：Andriod设备；4：iOS设备；5：Windows Phone设备；
//deploystatus 1:Development 2:Production
func (p *Pusher) Push(userid, channelid, msgkey, title, content string, devicetype, deploystatus int) error {
	//截取title,限制30字符长度
	r := []rune(title)
	if len(r) > 30 {
		title = string(r[0:30]) + "..."
	}

	//组装key集合
	keys := make(map[string]string)
	keys["method"] = "push_msg"
	keys["apikey"] = p.ApiKey
	keys["user_id"] = userid
	keys["channel_id"] = channelid
	keys["push_type"] = "3" //1, 单人; 2,tag, 3, 所有
	keys["timestamp"] = strconv.FormatInt(time.Now().Unix(), 10)
	keys["device_type"] = strconv.Itoa(devicetype)
	keys["msg_keys"] = msgkey

	if devicetype == 3 {
		keys["message_type"] = "0"
		keys["messages"] = p.makeAndMessage(title, content)
	} else if devicetype == 4 {
		keys["message_type"] = "1"
		keys["deploy_status"] = strconv.Itoa(deploystatus)
		keys["messages"] = p.makeIosMessage(title, content)
	} else {
		return errors.New("unsupported devicetype: " + strconv.Itoa(devicetype))
	}
	//获取签名
	p.GetSign(keys)
	//发送请求
	return p.PostUrl(keys)
}

func (p *Pusher) PostUrl(keys map[string]string) error {
	body := bytes.NewBufferString("")
	c := 0
	l := len(keys)
	for k, v := range keys {
		body.WriteString(k)
		body.WriteString("=")
		body.WriteString(v)
		c++
		if c < l {
			body.WriteString("&")
		}
	}
	resp, err := http.Post(p.Url, "application/x-www-form-urlencoded", body)
	defer resp.Body.Close()
	var respdata RespData
	if err != nil {
		er, _ := ioutil.ReadAll(resp.Body)
		return errors.New(err.Error() + string(er))
	}
	b, _ := ioutil.ReadAll(resp.Body)
	err2 := json.Unmarshal(b, &respdata)
	if err2 != nil {
		return err2
	}
	if respdata.Response_params.Success_amount < 1 {
		return errors.New("push failed")
	}
	return nil
}

func (p *Pusher) GetSign(keys map[string]string) {
	//获取签名
	httpMethod := "POST"
	buf := bytes.NewBufferString(httpMethod)
	buf.WriteString(p.Url)
	//排序
	l := make([]string, len(keys))
	for k, v := range keys {
		l = append(l, k+"="+v)
	}
	sort.Strings(l)
	for _, s := range l {
		buf.WriteString(s)
	}
	buf.WriteString(p.SercretKey)
	rUrl := url.QueryEscape(buf.String())
	sign := md5.Sum([]byte(rUrl))
	//写入签名key
	keys["sign"] = hex.EncodeToString(sign[:])
}

func (p *Pusher) makeIosMessage(title, content string) string {
	return fmt.Sprintf(`{"aps":{"alert":"%s","sound":"","badge":1},"key1":"%s"}`, title, content)
}

func (p *Pusher) makeAndMessage(title, content string) string {
	return fmt.Sprintf(`{"title":"%s","description":"","custom_content":{"key1":"%s"}}`, title, content)
}

//-------------------------------------
type RespData struct {
	Request_id      int       `request_id`
	Response_params RespParam `response_params`
	Error_code      int       `error_code`
	Error_msg       string    `error_msg`
}

type RespParam struct {
	Success_amount int `success_amount`
}
