//BEANSTALKD IOS CONSUMER

package main
import (
    "github.com/iwanbk/gobeanstalk"
    "log"
	"strings"
    "fmt"
	apns "github.com/anachronistic/apns"
)

func main() {
    conn, err := gobeanstalk.Dial("localhost:11300")
    if err != nil {
        log.Fatal(err)
    }
    for {
		conn.Watch("apns")
        j, err := conn.Reserve()
        if err != nil {
            log.Fatal(err)
        }
        log.Printf("id:%d, body:%s\n", j.ID, string(j.Body))
		
		//SPLIT DATA
		result := strings.Split(string(j.Body), "#####CONCAT#####")
		// Display all elements.
		for i := range result {
			fmt.Println("result #",i, result[i])
		}
		// Length
		fmt.Println(len(result))
		
		//PUSH NOTIFICATION PROCESS//
		pushtoIOS(result[1])
		fmt.Println("ios push : success")

		
        err = conn.Delete(j.ID) 
        if err != nil {
            log.Fatal(err)
        }
    }
}

func pushtoIOS(device_token string){
	payload := apns.NewPayload()
    payload.Alert = "intern:hellothere"
    payload.Badge = 1
    payload.Sound = "default"

    pn := apns.NewPushNotification()
    //pn.DeviceToken = "ff5407377561851dfd83d4e29a4f03b51b856edfd55b94329370ab4d1f177cdc";
    pn.DeviceToken = device_token

    pn.AddPayload(payload)
    client := apns.NewClient("gateway.sandbox.push.apple.com:2195", "PushDNS.pem", "PushDNSKey-noenc.pem")
    resp := client.Send(pn)

    alert, _ := pn.PayloadString()
    fmt.Println("Alert:", alert)
    fmt.Println("Success", resp.Success)
    fmt.Println("Error", resp.Error)
}