package main

import (
        "fmt"
        apns "github.com/anachronistic/apns"
)

func main() {
        payload := apns.NewPayload()
        payload.Alert = "Hello World"
        payload.Badge = 1
        payload.Sound = "default"

        pn := apns.NewPushNotification()
        //pn.DeviceToken = "ff5407377561851dfd83d4e29a4f03b51b856edfd55b94329370ab4d1f177cdc";
        pn.DeviceToken = "fe0a735a52f6053280ba410d1eb8c1dd1d893a1d3c21f050fe47c954a77e56e3";

        pn.AddPayload(payload)
        client := apns.NewClient("gateway.sandbox.push.apple.com:2195", "PushDNS.pem", "PushDNSKey-noenc.pem")
        resp := client.Send(pn)

        alert, _ := pn.PayloadString()
        fmt.Println("Alert:", alert)
        fmt.Println("Success", resp.Success)
        fmt.Println("Error", resp.Error)
}


